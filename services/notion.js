const dotenv = require('dotenv').config();
const {Client} = require('@notionhq/client');

//init client
const notion = new Client({
    auth: process.env.NOTION_TOKEN
});

//list databases
const listDatabases = async () => {
    const res = await notion.databases.list();
}
listDatabases();


//get database data
module.exports = async function getJournals() {

    const payload = {
        path: `databases/${process.env.NOTION_DATABASE_ID}/query`,
        method: 'POST'
    };

    const {results} = await notion.request(payload);
    console.log("== JOURNALS ==");
 
    const journals = results.map(page => {
        return {
            id: page.id,
            created: new Date(page.properties.Created.created_time).toLocaleDateString(),
            title: page.properties.Name.title[0].text.content,
            description: page.properties.Description.rich_text[0].text.content,
            // tags: page.properties.Tags.rich_text[0].text.content
            tags: page.properties.Tags.multi_select[0].name
        };
    });

    return journals;
};