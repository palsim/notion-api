const express = require('express');
const getJournals = require('./services/notion');
const PORT = process.env.PORT || 5001;

const app = express();

app.use(express.static('dist'));

app.get('/journals', async(req, res) => {
    const journals = await getJournals();
    res.json(journals);
});

app.listen(PORT, console.log(`Server started on port ${PORT}`));