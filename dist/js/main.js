const journalsElem = document.querySelector('#journals');

const getJournalsFromServer = async () => {
    const res = await fetch('http://localhost:5001/journals');
    const data = await res.json();    
    return data;
};

const addJournalsToDOM = async () => {
    const journals = await getJournalsFromServer();
    
    journals.forEach(journal => {
        const div = document.createElement('div');
        div.innerHTML = `
            <pre>
            <h1>${journal.title}</h1>
            <h2>${journal.created}</h2>
            <h3>${journal.description}</h3>
            <h4>${journal.tags}</h4>
            </pre>
        `;

        journalsElem.appendChild(div);
    })
};

addJournalsToDOM();