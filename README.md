# notion-api

Simple app to display database pages from [Notion](notion.so) using the new [Notion SDK for Javascript](https://github.com/makenotion/notion-sdk-js).

## Installation

```bash
npm intall
```

## Configuration

```bash
in the / folder create a .env file with this two vars:
NOTION_TOKEN="your notion token"
NOTION_DATABASE_ID="your notion databaseId"
```

## Usage

```bash
npm start
```

## License
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)